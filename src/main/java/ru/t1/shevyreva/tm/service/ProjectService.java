package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.service.IProjectService;
import ru.t1.shevyreva.tm.enumerated.Sort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.field.DescriptionEmptyException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.IndexEmptyException;
import ru.t1.shevyreva.tm.exception.field.NameEmptyException;
import ru.t1.shevyreva.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = new Project();
        project.setName(name);
        add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = projectRepository.findOneById(id);
        return project;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() <= index) throw new IndexEmptyException();
        final Project project = projectRepository.findOneByIndex(index);
        return project;
    }

    @Override
    public void remove(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public void removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        projectRepository.removeById(id);
    }

    @Override
    public void removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() <= index) throw new IndexEmptyException();
        projectRepository.removeByIndex(index);
    }

    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize() <= index) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index < 0 || index == null) throw new IndexEmptyException();
        if (getSize() <= index) throw new IndexEmptyException();
        if (status == null) throw new StatusNotFoundException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    public Project changeProjectStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    public boolean existsById(final String id) {
        return projectRepository.existsById(id);
    }

    public Integer getSize() {
        return projectRepository.getSize();
    }

}
