package ru.t1.shevyreva.tm.service;

import ru.t1.shevyreva.tm.api.repository.ICommandRepository;
import ru.t1.shevyreva.tm.api.service.ICommandService;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.exception.field.NameEmptyException;

import java.util.Collection;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
