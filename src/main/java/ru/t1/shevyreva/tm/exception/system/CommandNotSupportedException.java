package ru.t1.shevyreva.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException(String command) {
        super("Error! '" + command + "' is not supported! Enter help for list commands.");
    }

}
