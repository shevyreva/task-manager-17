package ru.t1.shevyreva.tm.command.system;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    private final String DESCRIPTION = "Show about program.";

    private final String NAME = "about";

    private final String ARGUMENT = "-a";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Shevyreva Liya");
        System.out.println("e-mail: liyavmax@gmail.com");
    }

}
