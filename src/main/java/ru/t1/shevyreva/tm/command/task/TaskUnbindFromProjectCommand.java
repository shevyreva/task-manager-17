package ru.t1.shevyreva.tm.command.task;

import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Unbind task from a project.";

    private final String NAME = "task-unbind-from-project";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().bindTaskToProject(projectId, taskId);
    }

}
