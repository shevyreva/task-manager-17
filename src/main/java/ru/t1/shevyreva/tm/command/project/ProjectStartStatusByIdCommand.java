package ru.t1.shevyreva.tm.command.project;

import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectStartStatusByIdCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Project start by Id.";

    private final String NAME = "project-start-by-id";

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusById(id, Status.IN_PROGRESS);
    }

}
