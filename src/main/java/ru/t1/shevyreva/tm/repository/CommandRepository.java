package ru.t1.shevyreva.tm.repository;

import ru.t1.shevyreva.tm.api.repository.ICommandRepository;
import ru.t1.shevyreva.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> mapByCommand = new LinkedHashMap<>();

    public AbstractCommand getCommandByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return mapByCommand.get(name);
    }

    public AbstractCommand getCommandByArgument(String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return mapByArgument.get(argument);
    }

    public void add(AbstractCommand command) {
        if (command == null) return;
        final String name = command.getName();
        if (name != null && !name.isEmpty()) mapByCommand.put(name, command);
        final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByCommand.values();
    }

}
